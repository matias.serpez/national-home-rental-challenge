# [0.3.5] 21-02-12

## Added

- Format code, lint.

# [0.3.4] 21-02-12

## Added

- Added delete functionality.

# [0.3.3] 21-02-12

## Added, Fixed

- Added filtering
- Added sorting
- Fixed react start script with flag SKIP_PREFLIGHT_CHECK=true. It crash sometimes after install eslint, probably a incompatibility with versions. TODO: Check it!
- Added form behavior and validations.

# [0.3.2] 21-02-12

## Fix, Added

- Fix Service `addTenant` return;
- Added Service interface.
- Added components directory.
- Added DAL (Data access layer) with hooks.
- Populated tenants data in table.
- Created `Table`, `Tabs` and `Indicator` components.

# [0.3.1] 21-02-12

## Added

- Added containers directory.

# [0.3.0] 21-02-12

## Added

- Added prettier and eslint.
- Format code.
- Added scripts to package.json

# [0.2.0] 21-02-12

## Added

- Added typescript dependencies.
- Changed file extensions `js` -> `ts` and `tsx`.
- Added types to Service.