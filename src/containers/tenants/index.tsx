import React, { useState } from 'react'
import { Table, Tabs, Form } from '../../components'
import { useTenants } from '../../dal'
import { useTabFilters } from './useTabFilters'
import { ITenant } from '../../Service'

function Tenant() {
  // TODO: Move it to context, useCtx, useReducer.
  const [tenants, setTenants] = useState<ITenant[]>([])
  const { error, loading } = useTenants(setTenants)
  const { filter, setFilter } = useTabFilters()
  const [formVisible, setFromVisible] = useState<boolean>(false)

  const showFormHandler = () => {
    setFromVisible(true)
  }

  return (
    <>
      <div className="container">
        <h1>Tenants</h1>
        <Tabs filter={filter} setFilter={setFilter} />
        <Table
          tenants={tenants}
          setTenants={setTenants}
          loading={loading}
          error={error}
          filter={filter}
        />
      </div>
      <div className="container">
        <button onClick={showFormHandler} className="btn btn-secondary">
          Add Tenant
        </button>
      </div>
      <div className="container">
        {formVisible && <Form setFromVisible={setFromVisible} setTenants={setTenants} />}
      </div>
    </>
  )
}

export default Tenant
