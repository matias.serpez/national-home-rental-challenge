import { useState } from 'react'
import { ITenant } from '../../Service'

export type SortDirection = 'ASC' | 'DESC'

export function useSorting() {
  const [criteriaKey, setCriteriaKey] = useState<keyof ITenant>('id')
  const [direction, setDirection] = useState<SortDirection>('ASC')

  return {
    criteriaKey,
    direction,
    setCriteriaKey,
    setDirection
  }
}
