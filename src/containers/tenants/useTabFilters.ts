import { useState } from 'react'
import { PaymentStatus } from '../../Service'

export type TabFilterType = PaymentStatus | 'ALL'

export function useTabFilters() {
  const [filter, setFilter] = useState<TabFilterType>('ALL')

  return {
    filter,
    setFilter
  }
}
