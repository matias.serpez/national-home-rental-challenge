import React from 'react'
import { render } from '@testing-library/react'
import Tenants from './'

test('renders learn react link', () => {
  const { getByText } = render(<Tenants />)
  const linkElement = getByText(/learn react/i)
  expect(linkElement).toBeInTheDocument()
})
