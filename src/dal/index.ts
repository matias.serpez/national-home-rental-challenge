// Data access layer
import { useState, useEffect } from 'react'
import { Service, ITenant } from '../Service'

export function useTenants(setTenants: (tenantsList: ITenant[]) => void) {
  const [loading, setLoading] = useState<boolean>(true)
  const [error, setError] = useState<string | null>(null)

  async function getData() {
    try {
      setLoading(true)
      const data = await Service.getTenants()
      setTenants(data)
    } catch (error) {
      setError(error.message)
    } finally {
      setLoading(false)
    }
  }

  useEffect(() => {
    getData()
  }, [])

  return {
    loading,
    error
  }
}

export function useAddTenant() {
  const [loading, setLoading] = useState<boolean>(false)
  const [error, setError] = useState<string | null>(null)

  async function addTenant(tenant: ITenant) {
    try {
      setLoading(true)
      return await Service.addTenant(tenant)
    } catch (error) {
      setError(error)
    } finally {
      setLoading(false)
    }
  }

  return {
    loading,
    error,
    addTenant
  }
}

export function useDeleteTenant() {
  const [loading, setLoading] = useState<boolean>(false)
  const [error, setError] = useState<string | null>(null)

  async function deleteTenant(id: number) {
    try {
      setLoading(true)
      return await Service.deleteTenant(id)
    } catch (error) {
      setError(error)
    } finally {
      setLoading(false)
    }
  }

  return {
    loading,
    error,
    deleteTenant
  }
}
