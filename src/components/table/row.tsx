import React from 'react'
import { ITenant } from '../../Service'

interface IProps {
  deleteTenant: (id: number) => void
  deleteLoading: boolean
  tenant: ITenant
}

export function Row({ deleteLoading, tenant, deleteTenant }: IProps) {
  const onDeleteHandler = () => {
    return window.confirm('Delete?') ? deleteTenant(tenant.id) : null
  }
  return (
    <tr>
      <th>{tenant.id}</th>
      <td>{tenant.name}</td>
      <td>{tenant.paymentStatus}</td>
      <td>{tenant.leaseEndDate}</td>
      <td>
        <button onClick={onDeleteHandler} disabled={deleteLoading} className="btn btn-danger">
          Delete
        </button>
      </td>
    </tr>
  )
}
