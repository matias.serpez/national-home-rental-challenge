// TODO: make it generic.
import React from 'react'
import _sortBy from 'lodash/sortBy'
import { ITenant } from '../../Service'

import { Row } from './row'
import { Indicator } from '../indicator'
import { TabFilterType } from '../../containers/tenants/useTabFilters'
import { useSorting } from '../../containers/tenants/useSorting'
import { useDeleteTenant } from '../../dal'

interface IProps {
  tenants: ITenant[]
  setTenants: (tenants: ITenant[]) => void
  error: string | null
  loading: boolean
  filter: TabFilterType
}

export function Table({ tenants, error, loading, filter, setTenants }: IProps) {
  // Sorting
  const { criteriaKey, direction, setCriteriaKey, setDirection } = useSorting()
  // Deleteion
  const { deleteTenant, error: deleteError, loading: deleteLoading } = useDeleteTenant()

  if (error || loading) return <Indicator error={error} loading={loading} />

  // If list is too large, implement useMemo.
  const filteredTenants = tenants.filter((tenant) => {
    if (filter === 'ALL') return true
    return tenant.paymentStatus === filter
  })

  const orderTenants = () => {
    const sortedList: ITenant[] = _sortBy(filteredTenants, criteriaKey)
    if (direction === 'DESC') sortedList.reverse()
    return sortedList
  }

  /**
   * If sortKey (column) is the same, change direction.
   * If the user click in other column, reset direction to 'ASC'
   * and set new sort criteria Key.
   * @param sortKey
   */
  const setOrdering = (sortKey: keyof ITenant) => () => {
    if (criteriaKey === sortKey) {
      setDirection((oldDir) => (oldDir === 'ASC' ? 'DESC' : 'ASC'))
    } else {
      setCriteriaKey(sortKey)
      setDirection('ASC')
    }
  }

  // Delete handler
  const onDeleteHandler = async (id: number) => {
    const result = await deleteTenant(id)
    if (result) {
      alert('Success')
      setTenants(tenants.filter((item) => item.id !== id))
    }
  }

  // Row with props to send to map
  const FullRow = (tenant: ITenant) => {
    return <Row deleteLoading={deleteLoading} deleteTenant={onDeleteHandler} tenant={tenant} />
  }

  const orderedTenants = orderTenants()

  return (
    <table className="table">
      <Indicator error={deleteError} />
      <thead>
        <tr>
          <th onClick={setOrdering('id')}>#</th>
          <th onClick={setOrdering('name')}>Name</th>
          <th onClick={setOrdering('paymentStatus')}>Payment Status</th>
          <th onClick={setOrdering('leaseEndDate')}>Lease End Date</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>{orderedTenants.map(FullRow)}</tbody>
    </table>
  )
}
