import React from 'react'

interface IProps {
  loading?: boolean
  error?: string | null
}

export function Indicator({ loading, error }: IProps) {
  if (!error && !loading) return null
  return (
    <div>
      {loading && <h1>Loading...</h1>}
      {error && <h1>{`Error: ${error}`}</h1>}
    </div>
  )
}
