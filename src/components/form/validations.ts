import moment from 'moment'

export function validateName(value?: string) {
  if (!value) return 'Required'
  if (value && value.length > 25) return 'Should be max 25 characters'
  return undefined
}

export function validateDate(value?: string) {
  if (!value) return 'Required'
  const momentDate = moment(value)
  if (!momentDate.isValid()) return 'Invalid Date'
  if (momentDate.isBefore(moment())) return 'Invalid Date, should be future'
  return undefined
}
