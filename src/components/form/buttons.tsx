import React from 'react'
import { useFormikContext } from 'formik'

interface IProps {
  setFromVisible: (visible: boolean) => void
  disabled: boolean
}

export function Buttons({ setFromVisible, disabled }: IProps) {
  const { resetForm, isSubmitting } = useFormikContext()

  const onCancelHandler = () => {
    resetForm()
    setFromVisible(false)
  }

  return (
    <>
      <button disabled={disabled || isSubmitting} className="btn btn-primary">
        Save
      </button>
      <button disabled={disabled || isSubmitting} className="btn" onClick={onCancelHandler}>
        Cancel
      </button>
    </>
  )
}
