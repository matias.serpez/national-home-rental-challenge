import React from 'react'
import { Formik, Form as FormikForm, Field } from 'formik'

import { Buttons } from './buttons'

import { ITenant } from '../../Service'
import { useAddTenant } from '../../dal'
import { Indicator } from '../indicator'
import { validateName, validateDate } from './validations'

interface IProps {
  setTenants: (tenants: ITenant[]) => void
  setFromVisible: (visible: boolean) => void
}

export function Form({ setFromVisible, setTenants }: IProps) {
  const { addTenant, error, loading } = useAddTenant()

  // TODO: Check this any type.
  const onSubmitHandler = async (data: ITenant, { resetForm }: any) => {
    const tenants = await addTenant(data)

    if (tenants) {
      setTenants(tenants)
      setFromVisible(false)
    }

    resetForm()
  }

  return (
    <Formik<ITenant>
      initialValues={{
        id: 0,
        leaseEndDate: '',
        name: '',
        paymentStatus: 'CURRENT'
      }}
      onSubmit={onSubmitHandler}>
      {({ errors }) => {
        return (
          <FormikForm>
            <div>{!!error && <Indicator error={error} />}</div>
            <div className="form-group">
              <label>Name</label>
              <Field as="input" className="form-control" name="name" validate={validateName} />
              {errors.name && <p className="hasError error">{errors.name}</p>}
            </div>
            <div className="form-group">
              <label>Payment Status</label>
              <Field as="select" className="form-control" name="paymentStatus">
                <option>CURRENT</option>
                <option>LATE</option>
              </Field>
            </div>
            <div className="form-group">
              <label>Lease End Date</label>
              <Field
                type="date"
                as="input"
                className="form-control"
                name="leaseEndDate"
                validate={validateDate}
              />
              {errors.leaseEndDate && <p className="hasError error">{errors.leaseEndDate}</p>}
            </div>
            <Buttons setFromVisible={setFromVisible} disabled={loading} />
          </FormikForm>
        )
      }}
    </Formik>
  )
}
