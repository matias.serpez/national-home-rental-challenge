// TODO: make it generic.
import React from 'react'
import { TabFilterType } from '../../containers/tenants/useTabFilters'

interface IProps {
  filter: TabFilterType
  setFilter: (filter: TabFilterType) => void
}

// TODO: handle by URL.
export function Tabs({ filter, setFilter }: IProps) {
  const onSetFilterHandler = (selectedFilter: TabFilterType) => () => {
    setFilter(selectedFilter)
  }

  return (
    <ul className="nav nav-tabs">
      <li className="nav-item">
        <a
          onClick={onSetFilterHandler('ALL')}
          className={`nav-link ${filter === 'ALL' && 'active'}`}
          href="#">
          All
        </a>
      </li>
      <li className="nav-item">
        <a
          onClick={onSetFilterHandler('LATE')}
          className={`nav-link ${filter === 'LATE' && 'active'}`}
          href="#">
          Payment is late
        </a>
      </li>
      <li className="nav-item">
        <a
          onClick={onSetFilterHandler('CURRENT')}
          className={`nav-link ${filter === 'CURRENT' && 'active'}`}
          href="#">
          Lease ends in less than a month
        </a>
      </li>
    </ul>
  )
}
