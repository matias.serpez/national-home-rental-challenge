import React from 'react'
import ReactDOM from 'react-dom'
import Tenant from './containers/tenants'
import 'bootstrap/dist/css/bootstrap.css'

ReactDOM.render(
  <React.StrictMode>
    <Tenant />
  </React.StrictMode>,
  document.getElementById('root')
)
